# ~/.bashrc

[[ $- != *i* ]] && return

alias ls='ls --color=auto'
PS1='\[$(tput setaf 11)\][\[$(tput setaf 9)\]\t \[$(tput setaf 11)\]> \[$(tput setaf 10)\]\u \[$(tput setaf 11)\]> \[$(tput setaf 12)\]\W\[$(tput setaf 11)\]]\$ \[$(tput setaf 15)\]'

if [[ $(ls /dev/pts/ | wc -l) -eq 2 ]]; then 
	neofetch
fi
