-- ~/.config/xmobar/xmobar.hs

Config { 

	-- appearance

		font = "xft:SauceCodePro Nerd Font Mono:size=10:antialias=true,Symbols Nerd Font:size=10"

		, bgColor = "#02122B"
		, fgColor = "#729fcf"

		, alpha = 255

		, position = Static { xpos = 0 , ypos = 0 , width = 1920 , height = 22 }



	-- layout

		, sepChar  = "%" 
		, alignSep = "}{"

		, template = "          |          | %date% }{ %cpu% | %cputemp% | %memory%%memtotal% | %battery% "



	-- behavior

		, lowerOnStart = True
		, persistent   = True



	-- plugins

		, commands = [ 


		  Run Date "%A | %d/%m/%Y | %H:%M:%S" "date" 10
		
		, Run Cpu 		[ "--template" 	, "CPU: <bar>"
					, "--bwidth"	, "10"
					, "--Low"	, "25"
					, "--High" 	, "75"
					, "--low"	, "#8ae234"
					, "--normal"	, "#fce94f"
					, "--high" 	, "#ef2929"
					] 4
		
                , Run Com "/bin/bash"	["-c"           , "/home/frtz/scripts/xmobar/cputemp.sh"] "cputemp" 40  
							  -- execute command to get package temperature

		, Run Com "/bin/bash"	["-c"		, "free --mebi | awk '{print $2}' | awk '$0' | tr -dc '0-9' | echo $(cat -) MiB"] "memtotal" 86400  
							  -- execute command to get total memory
		
		, Run Memory 		[ "--template"  , "RAM: <used>/"
					, "--Low"       , "973"
					, "--High"      , "3892"
					, "--low"       , "#8ae234"
					, "--normal"    , "#fce94f"
					, "--high"      , "#ef2929"
					] 40

		, Run Battery        	[ "--template" 	, "BATT: <acstatus>"
		                        , "--Low"      	, "20"
		                        , "--High"     	, "80"
		                        , "--low"      	, "#ef2929"
		                        , "--normal"   	, "#fce94f"
		                        , "--high"     	, "#8ae234"

		                        , "--" 

		                        , "-o"	, "<left>% (<timeleft>h)"
		                        , "-O"	, "<fc=#8ae234>Charging</fc>"
		                        , "-i"	, "<fc=#729fcf>Charged</fc>"
		                        ] 40


		]

}
