-- ~/.xmonad/xmonad.hs



-- import

import XMonad

import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers

import XMonad.Layout.Magnifier
import XMonad.Layout.Spacing
import XMonad.Layout.ThreeColumns

import XMonad.Util.Run
import XMonad.Util.SpawnOnce

import Data.Monoid

import System.Exit


import qualified XMonad.StackSet as W

import qualified Data.Map as M

import qualified DBus as D
import qualified DBus.Client as DC

import qualified Codec.Binary.UTF8.String as UTF8



-- main

main :: IO ()
main = do
	xmproc <- spawnPipe "xmobar /home/frtz/.config/xmobar/xmobar.hs" 
	xmproc <- spawnPipe "polybar -c /home/frtz/.config/polybar/config.ini"
	xmonad $ ewmh $ defaults



-- defaults

defaults = def 
	{ borderWidth		= myBorderWidth
	, clickJustFocuses	= myClickJustFocuses
	, focusedBorderColor    = myFocusedBorderColor
	, focusFollowsMouse     = myFocusFollowsMouse
        , keys                  = myKeys
        , layoutHook            = myLayout
	, manageHook		= myManageHook
	, modMask 		= myModMask
        , mouseBindings         = myMouseBindings
        , normalBorderColor     = myNormalBorderColor
	, terminal 		= myTerminal
	, workspaces 		= myWorkspaces 
	}



myModMask = mod4Mask



-- layouts 

myLayout = spacingRaw False (Border 25 3 3 3) True (Border 3 3 3 3) True $ avoidStruts $
	Tall 1 (3/100) (1/2)
    |||	Mirror (Tall 1 (3/100) (1/2))
    |||	Full
    |||	ThreeColMid 1 (3/100) (3/7)



-- appearance

myTerminal = "alacritty"

myBorderWidth = 2 

myNormalBorderColor  = "#02122B"
myFocusedBorderColor = "#146273"

myWorkspaces    = ["1","2","3","4","5","6","7","8","9"]



-- focus

myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

myClickJustFocuses :: Bool
myClickJustFocuses = False



-- keybindings

myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $
	[ ((modm .|. shiftMask, xK_Return), spawn $ XMonad.terminal conf)
	, ((modm,               xK_p     ), spawn "dmenu_run")
	, ((modm .|. shiftMask, xK_p     ), spawn "/home/frtz/scripts/dmenu/usermenu.sh")
	, ((modm .|. shiftMask, xK_c     ), kill)
	, ((modm,               xK_space ), sendMessage NextLayout)
	, ((modm .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf)
	, ((modm,               xK_n     ), refresh)
	, ((modm,               xK_Tab   ), windows W.focusDown)
	, ((modm .|. shiftMask, xK_Tab   ), windows W.focusUp)
	, ((modm,               xK_j     ), windows W.focusDown)
	, ((modm,               xK_k     ), windows W.focusUp)
	, ((modm,               xK_m     ), windows W.focusMaster) 
	, ((modm,               xK_Return), windows W.swapMaster)
	, ((modm .|. shiftMask, xK_j     ), windows W.swapDown)
	, ((modm .|. shiftMask, xK_k     ), windows W.swapUp)
	, ((modm,               xK_h     ), sendMessage Shrink)
	, ((modm,               xK_l     ), sendMessage Expand)
	, ((modm,               xK_t     ), withFocused $ windows . W.sink)
	, ((modm,               xK_comma ), sendMessage (IncMasterN 1))
	, ((modm,               xK_period), sendMessage (IncMasterN (-1)))
	, ((modm .|. shiftMask, xK_q     ), io (exitWith ExitSuccess))
	, ((modm,               xK_q     ), spawn "xmonad --recompile; killall --wait polybar xmobar; xmonad --restart")
	, ((0,                 0x1008ff02), spawn "xbacklight -inc 5") 
	, ((0,                 0x1008ff03), spawn "xbacklight -dec 5")
        , ((0,                 0x1008ff12), spawn "amixer set Master toggle")
        , ((0,                 0x1008ff13), spawn "amixer -M set Master 5%+")
        , ((0,                 0x1008ff11), spawn "amixer -M set Master 5%-")
	]
	++
	[((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]



-- mousebindings

myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $
	[ ((modm, button1), (\w -> focus w >> mouseMoveWindow w >> windows W.shiftMaster))
	, ((modm, button3), (\w -> focus w >> mouseResizeWindow w >> windows W.shiftMaster))
	]



-- 

myManageHook = composeAll
	[ className =? "Polybar" --> doIgnore
	]
